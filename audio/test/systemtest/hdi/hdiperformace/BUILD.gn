# Copyright (c) 2021-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

hdf_audio_path = "./../../../.."
hdf_audio_test_path = "./../.."

if (defined(ohos_lite)) {
  import("//build/lite/config/test.gni")
} else {
  import("//build/test.gni")
}
import("$hdf_audio_path/audio.gni")

if (defined(ohos_lite)) {
  moduletest("hdf_audio_hdi_render_performace_test") {
    sources = [
      "$hdf_audio_test_path/common/hdi_common/src/audio_hdi_common.cpp",
      "src/audio_hdirender_performace_test.cpp",
    ]

    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "$hdf_audio_path/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "$hdf_audio_test_path/common/hdi_common/include",
      "//third_party/googletest/googletest/include/gtest",
      "$hdf_audio_test_path//hdi/hdiperformace/include",
    ]
    public_deps = [
      "$hdf_audio_path/hal/hdi_passthrough:hdi_audio",
      "//drivers/hdf_core/adapter/uhdf/manager:hdf_core",
      "//drivers/hdf_core/adapter/uhdf/posix:hdf_posix_osal",
    ]
    if (!drivers_peripheral_audio_feature_rich_device) {
      defines = [ "FEATURE_SMALL_DEVICE" ]
    }
  }

  moduletest("hdf_audio_hdi_capture_performace_test") {
    sources = [
      "$hdf_audio_test_path/common/hdi_common/src/audio_hdi_common.cpp",
      "src/audio_hdicapture_performace_test.cpp",
    ]

    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "$hdf_audio_path/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "$hdf_audio_test_path/common/hdi_common/include",
      "//third_party/googletest/googletest/include/gtest",
      "$hdf_audio_test_path/hdi/hdiperformace/include",
    ]
    public_deps = [
      "$hdf_audio_path/hal/hdi_passthrough:hdi_audio",
      "//drivers/hdf_core/adapter/uhdf/manager:hdf_core",
      "//drivers/hdf_core/adapter/uhdf/posix:hdf_posix_osal",
    ]
    if (!drivers_peripheral_audio_feature_rich_device) {
      defines = [ "FEATURE_SMALL_DEVICE" ]
    }
  }
} else {
  module_output_path = "drivers_peripheral_audio/audio"

  ohos_systemtest("hdf_audio_hdi_render_performace_test") {
    module_out_path = module_output_path
    resource_config_file = "./../../../resource/ohos_test.xml"
    sources = [
      "$hdf_audio_test_path/common/hdi_common/src/audio_hdi_common.cpp",
      "src/audio_hdirender_performace_test.cpp",
    ]

    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "$hdf_audio_path/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "$hdf_audio_test_path/common/hdi_common/include",
      "//third_party/googletest/googletest/include/gtest",
      "$hdf_audio_test_path/hdi/hdiperformace/include",
    ]

    deps = [
      "//third_party/googletest:gmock_main",
      "//third_party/googletest:gtest_main",
    ]
    external_deps = [ "hdf_core:libhdf_utils" ]
    defines = []
    if (enable_audio_adm_passthrough) {
      deps += [ "$hdf_audio_path/hal/hdi_passthrough:hdi_audio" ]
      defines += [ "AUDIO_ADM_SO" ]
    }
    if (enable_audio_adm_service) {
      deps += [ "$hdf_audio_path/hal/hdi_binder/proxy:hdi_audio_client" ]
      defines += [ "AUDIO_ADM_SERVICE" ]
    }
    if (!drivers_peripheral_audio_feature_rich_device) {
      defines += [ "FEATURE_SMALL_DEVICE" ]
    }
  }

  ohos_systemtest("hdf_audio_hdi_capture_performace_test") {
    module_out_path = module_output_path
    sources = [
      "$hdf_audio_test_path/common/hdi_common/src/audio_hdi_common.cpp",
      "src/audio_hdicapture_performace_test.cpp",
    ]

    include_dirs = [
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "$hdf_audio_path/interfaces/include",
      "//third_party/bounds_checking_function/include",
      "$hdf_audio_test_path/common/hdi_common/include",
      "//third_party/googletest/googletest/include/gtest",
      "$hdf_audio_test_path/hdi/hdiperformace/include",
    ]

    deps = [
      "//third_party/googletest:gmock_main",
      "//third_party/googletest:gtest_main",
    ]
    external_deps = [ "hdf_core:libhdf_utils" ]
    defines = []
    if (enable_audio_adm_passthrough) {
      deps += [ "$hdf_audio_path/hal/hdi_passthrough:hdi_audio" ]
      defines += [ "AUDIO_ADM_SO" ]
    }
    if (enable_audio_adm_service) {
      deps += [ "$hdf_audio_path/hal/hdi_binder/proxy:hdi_audio_client" ]
      defines += [ "AUDIO_ADM_SERVICE" ]
    }
    if (!drivers_peripheral_audio_feature_rich_device) {
      defines += [ "FEATURE_SMALL_DEVICE" ]
    }
  }
}
group("hdi_performace") {
  if (!defined(ohos_lite)) {
    testonly = true
  }
  deps = [
    ":hdf_audio_hdi_capture_performace_test",
    ":hdf_audio_hdi_render_performace_test",
  ]
}
