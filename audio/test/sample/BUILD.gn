# Copyright (c) 2022-2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

hdf_audio_path = "./../.."

import("//build/ohos.gni")
import("$hdf_audio_path/audio.gni")

if (defined(ohos_lite)) {
  ohos_executable("audio_sample_render") {
    include_dirs = [
      "$hdf_audio_path/interfaces/include",
      "./",
      "//third_party/bounds_checking_function/include",
    ]

    sources = [
      "framework_common.c",
      "framework_render.c",
    ]

    deps = [ "//third_party/bounds_checking_function:libsec_shared" ]
    external_deps = [ "hdf_core:libhdf_utils" ]
    defines = []
    install_enable = true
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_executable("audio_sample_capture") {
    include_dirs = [
      "$hdf_audio_path/interfaces/include",
      "./",
      "//third_party/bounds_checking_function/include",
      "$hdf_audio_path/hal/hdi_binder/server/include",
    ]

    sources = [
      "framework_capture.c",
      "framework_common.c",
    ]

    deps = [ "//third_party/bounds_checking_function:libsec_shared" ]
    external_deps = [ "hdf_core:libhdf_utils" ]
    defines = []
    install_enable = true
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
} else {
  ohos_executable("audio_sample_render") {
    include_dirs = [
      "$hdf_audio_path/interfaces/include",
      "./",
    ]

    sources = [
      "framework_common.c",
      "framework_render.c",
    ]

    defines = []

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    install_enable = true
    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_executable("audio_sample_capture") {
    include_dirs = [
      "$hdf_audio_path/interfaces/include",
      "$hdf_audio_path/hal/hdi_binder/server/include",
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "./",
    ]

    sources = [
      "$hdf_audio_path/hal/hdi_binder/server/src/hdf_audio_events.c",
      "framework_capture.c",
      "framework_common.c",
    ]

    defines = []

    if (drivers_peripheral_audio_feature_alsa_lib) {
      defines += [ "ALSA_LIB_MODE" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    install_enable = true
    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
  ohos_executable("audio_sample_event") {
    include_dirs = [
      "$hdf_audio_path/interfaces/include",
      "$hdf_audio_path/hal/hdi_passthrough/include",
      "$hdf_audio_path/hal/hdi_binder/server/include",
    ]

    sources = [
      "$hdf_audio_path/hal/hdi_binder/server/src/hdf_audio_events.c",
      "framework_event.c",
    ]

    defines = []

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdf_utils",
        "hdf_core:libhdi",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    install_enable = true
    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_executable("idl_render") {
    include_dirs = [ "./" ]

    sources = [
      "framework_common.c",
      "idl_render.c",
    ]

    defines = [ "IDL_SAMPLE" ]
    if (drivers_peripheral_audio_feature_alsa_lib) {
      defines += [ "ALSA_LIB_MODE" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "drivers_interface_audio:libaudio_proxy_1.0",
        "hdf_core:libhdf_ipc_adapter",
        "hdf_core:libhdf_utils",
        "hdf_core:libhdi",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    install_enable = true
    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }

  ohos_executable("idl_capture") {
    include_dirs = [ "./" ]

    sources = [
      "framework_common.c",
      "idl_capture.c",
    ]

    defines = [ "IDL_SAMPLE" ]
    if (drivers_peripheral_audio_feature_alsa_lib) {
      defines += [ "ALSA_LIB_MODE" ]
    }

    if (is_standard_system) {
      external_deps = [
        "c_utils:utils",
        "drivers_interface_audio:libaudio_proxy_1.0",
        "hdf_core:libhdf_utils",
        "hiviewdfx_hilog_native:libhilog",
      ]
    } else {
      external_deps = [ "hilog:libhilog" ]
    }

    install_enable = true
    install_images = [ chipset_base_dir ]
    subsystem_name = "hdf"
    part_name = "drivers_peripheral_audio"
  }
}

group("audio_sample") {
  if (drivers_peripheral_audio_feature_hdf_proxy_stub == true) {
    deps = [
      ":audio_sample_capture",
      ":audio_sample_render",
    ]
  }
}

group("idl_audio_sample") {
  if (!defined(ohos_lite)) {
    if (drivers_peripheral_audio_feature_hdf_proxy_stub == true) {
      deps = [
        ":idl_capture",
        ":idl_render",
      ]
    }
  }
}
